SB3 Retriever
---------------------

# Setup

1. Download latest selenium standalone server
```sh
curl -O http://selenium-release.storage.googleapis.com/3.5/selenium-server-standalone-3.5.3.jar
```

2. Download the latest version geckodriver for your environment and unpack it in your project directory
```sh
# for Linux 64 bit
curl -L https://github.com/mozilla/geckodriver/releases/download/v0.16.0/geckodriver-v0.16.0-linux64.tar.gz | tar xz
```

3. Start selenium standalone server
```sh
java -jar -Dwebdriver.gecko.driver=./geckodriver selenium-server-standalone-3.5.3.jar
```

4. Keep this running in the background and open a new terminal window. Next step is to download WebdriverIO via NPM
```sh
npm install webdriverio
```

5. Run the program
```sh
node main.js
```
For more details, please refer to: http://webdriver.io/guide.html


# Updating Javascript dependencies
All JS dependencies are from `scratch-vm/plaground` directory
After run `npm install` and `npm run build`