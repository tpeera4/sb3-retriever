var fs = require('fs');
var webdriverio = require('webdriverio');

var dir = './output';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}


var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};

var path = process.cwd();
var browser = webdriverio
    .remote(options)
    .init();

browser.url("file://" + path + "/main.html")
    .element('#projectID')
    .setValue("123456789")
    .element('#processButton').click().pause(4000)
    .element('#output')
    .getValue()
    .then(function(output) {
        console.log('Output is: ' + output);
    
        fs.writeFile(dir+"/project-01.json", output, function(err) {
        if(err) {
            return console.log(err);
        }

            console.log("The file was saved!");
        });  
    })
    .end()
    .catch(function(err) {
        console.log(err);
    });
